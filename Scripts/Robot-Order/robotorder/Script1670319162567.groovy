import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.openqa.selenium.Rectangle as Rectangle
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory

'open Browser'
WebUI.openBrowser('https://robocorp.com/docs/courses/build-a-robot')

'maximize demo.nopcommerce '
WebUI.maximizeWindow()
WebUI.scrollToElement(findTestObject('Object Repository/Robot-order/a_Download'), 1)

WebUI.click(findTestObject('Object Repository/Robot-order/a_linkFile'))
WebUI.delay(5)
File download = new File('C:\\Users\\vthang\\Downloads')
List name = Arrays.asList(download.list())
if (name.contains('orders.csv')) {
	println "success"
}
else {
	println "fail"
}



'open Browser'
WebUI.openBrowser('https://robotsparebinindustries.com/#/robot-order')

'maximize demo.nopcommerce '
WebUI.maximizeWindow()

Date today = new Date()
String dateFolder = today.format('dd.MM.yyyy').replace('.', '-')
String nowTime = today.format('dd.MM.yyyy hh:mm:ss').replace(':', '.')
'read file csv'
List<String> ListItem1= CustomKeywords.'common.getProfiles.getExcel'('C:\\Users\\vthang\\Downloads\\orders.csv')

for(ii=0;ii<ListItem1.size();ii++) {
	
	'click ok'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_ok'))
	
	'select head'
	WebUI.selectOptionByValue(findTestObject("Object Repository/Robot-order/select_head"),ListItem1[ii].get(1), false)

	'input body'
	WebUI.click(findTestObject('Object Repository/Robot-order/div_body',[("value"):ListItem1[ii].get(2)]))

	'input legs_number'
	WebUI.setText(findTestObject('Object Repository/Robot-order/input_legs',["value":"1"]), ListItem1[ii].get(3))

	'input legs_address'
	WebUI.setText(findTestObject('Object Repository/Robot-order/input_legs',["value":"2"]), ListItem1[ii].get(4))
	'click button'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_proview'))
	'click button'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_orders'))
	
	result = CustomKeywords.'common.verify.verifyObjectNotPresent'(findTestObject('Object Repository/Robot-order/div_message'))
	while(result==false) {
	'click button'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_orders'))
	result = CustomKeywords.'common.verify.verifyObjectNotPresent'(findTestObject('Object Repository/Robot-order/div_message'))
	}

	WebUI.takeElementScreenshot((((((((((('ScreenShotssss' )+'/')+'/') + dateFolder)+'/') + '/') + 'SS'+ii) + '_') +nowTime) + '.png'),findTestObject('Object Repository/Robot-order/img_robotorder'))
	'click button'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_order-another'))

}
WebUI.closeBrowser()
////csvReader.close();
//
//
//
